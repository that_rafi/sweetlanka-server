// VALIDATION
const Joi = require('@hapi/joi');
const { join } = require('path');

// User VALIDATION

const userValidation = (users) => {
  const schema = Joi.object(
    {
      username : Joi.string()
      .min(6)
      .required(),
      email : Joi.string()
      .min(6)
      .required()
      .email(),
      password : Joi.string()
      .min(6)
      .required()
    }
  );

  return schema.validate(users);
};

const loginValidation = (data) => {
  const schema = Joi.object(
    {
      email : Joi.string()
      .min(6)
      .required()
      .email(),
      password : Joi.string()
      .min(6)
      .required()
    }
  );

  return schema.validate(data);
};

const intentValidation = (data) => {
  const schema = Joi.object(
    {
      name : Joi.string()
      .min(2)
      .required(),
      training_data : Joi.array(),
      response : Joi.array()
    }
  );
  return schema.validate(data)
}

const destinationValidation = (data) => {
  const schema = Joi.object(
    {
      name : Joi.string()
      .required(),
      desc : Joi.string(),
      location : Joi.string()
      .required(),
      images : Joi.array()
    }
  );

  return schema.validate(data);
};

const articleValidation = (data) => {
  const schema = Joi.object(
    {
      title : Joi.string()
      .required(),
      desc : Joi.string()
      .required(),
      author : Joi.string()
      .required(),
      images : Joi.array()
    }
  );

  return schema.validate(data);
};

const packagetourValidation = (data) => {
  const schema = Joi.object({
    name : Joi.string()
    .required(),
    desc : Joi.string()
    .required(),
    facility : Joi.string()
    .required(),
    base_price : Joi.number()
    .required(),
    categoryId : Joi.string()
    .required(),
    hotel_star : Joi.string()
    .required(),
    tags : Joi.string()
    .required(),
    start_date : Joi.date()
    .required(),
    status : Joi.required(),
    pickup_location : Joi.string()
    .min(0),
    itinerary : Joi.array()
  })
  return schema.validate(data)
}

const packageCategoryValidation = (data) => {
  const schema = Joi.object({
    name : Joi.string()
    .required(),
    max_person : Joi.number()
    .required(),
    transportation : Joi.string()
    .required()
  })

  return schema.validate(data)
}

const reservationValidation = (data) => {
  const schema = Joi.object({
    customer : Joi.required(),
    customerId : Joi.required(),
    packageId : Joi.required(),
    fullname : Joi.string()
    .required(),
    email : Joi.string()
    .email()
    .required(),
    location : Joi.string()
    .required(),
    phone_number : Joi.string()
    .required(),
    start_date : Joi.date()
    .required(),
    duration : Joi.number()
    .required(),
    no_adult : Joi.number()
    .required(),
    no_child : Joi.number()
    .required(),
    preferences : Joi.string()
    .required(),
    notes : Joi.required(),
    status : Joi.string()
    .required()
  })
  return schema.validate(data)
}

// export one by one function use this pattern
// module.exposts.function = function
module.exports.userValidation = userValidation;
module.exports.loginValidation = loginValidation;
module.exports.intentValidation = intentValidation;
module.exports.destinationValidation = destinationValidation;
module.exports.packagetourValidation = packagetourValidation;
module.exports.packageCategoryValidation = packageCategoryValidation;
module.exports.reservationValidation = reservationValidation;
module.exports.articleValidation = articleValidation;
