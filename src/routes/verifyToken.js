const jwt = require('jsonwebtoken')

module.exports = function (req,res,next){
  const token = req.header('auth-user')
  if(!token) return res.status(401).send('Access Denied!')

  try{
    const verified = jwt.verify(token,process.env.TOKEN_SECRET)
    req.user = verified // reveal data stored in jwt
    next(); // important
  }catch(err){
    res.status(401).send('Invalid Token!')
  }

}
