import express from "express"
let router = express.Router();

import usersController from "../controllers/usersController"
import intentController from "../controllers/intentController"
import destinationController from "../controllers/destinationController"
import packagetourController from "../controllers/packagetourController"
import packagecategoryController from "../controllers/package_categoryController"
import reservationController from "../controllers/reservationController"
import articleController from "../controllers/articleController"


const initWebRoutes = (app) => {
    // users
    app.get("/api/users",  usersController.getUsers )
    app.get("/api/users/:userId", usersController.getUsersByName)
    app.post("/api/users", usersController.addUser)
    app.delete("/api/users/:userId", usersController.deleteUser)
    app.patch("/api/users/:userId",usersController.updateUser)
    app.post("/api/users/login",usersController.loginUser)
    // intent
    app.get("/api/intent", intentController.getIntents)
    app.post("/api/intent",intentController.addIntent)
    app.delete("/api/intent/:intentId",intentController.deleteIntent)
    app.post("/api/test", intentController.testIntent)
    app.patch("/api/intent/:intentId", intentController.updateIntent)
    //Destination
    app.get("/api/destination",destinationController.getDestinations)
    app.get("/api/destination/:destinationId",destinationController.getDestinationById)
    app.post("/api/destination",destinationController.addDestination)
    app.delete("/api/destination/:destinationId", destinationController.deleteDestination)
    app.patch("/api/destination/:destinationId",destinationController.updateDestination)
    // Packagetour
    app.get("/api/packagetour",packagetourController.getPackagetours)
    app.get("/api/packagetour/:packagetourId",packagetourController.getPackagetourById)
    app.post("/api/packagetour",packagetourController.addPackagetour)
    app.delete("/api/packagetour/:packagetourId", packagetourController.deletePackagetour)
    app.patch("/api/packagetour/:packagetourId",packagetourController.updatePackagetour)

    // Package Category
    app.get("/api/package-category",packagecategoryController.getPackageCategorys)
    app.get("/api/package-category/:categoryId",packagecategoryController.getPackageCategoryById)
    app.post("/api/package-category",packagecategoryController.addPackageCategory)
    app.delete("/api/package-category/:categoryId", packagecategoryController.deletePackageCategory)
    app.patch("/api/package-category/:categoryId",packagecategoryController.updatePackageCategory)

    // Reservation
    app.get("/api/reservation",reservationController.getReservations)
    app.get("/api/reservation/:reservationId",reservationController.getReservationById)
    app.post("/api/reservation",reservationController.validateForm,reservationController.addReservation)
    app.delete("/api/reservation/:reservationId", reservationController.deleteReservation)
    app.patch("/api/reservation/:reservationId",reservationController.updateReservation)
    app.post("/api/recommendation", reservationController.getRecommendation)

    //Article
    app.get("/api/article",articleController.getArticles)
    app.get("/api/article/:articleId",articleController.getArticleById)
    app.post("/api/article",articleController.addArticle)
    app.delete("/api/article/:articleId", articleController.deleteArticle)
    app.patch("/api/article/:articleId",articleController.updateArticle)
    
    return app.use("/",router);
}

module.exports = initWebRoutes;