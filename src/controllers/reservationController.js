require('dotenv')
const Reservation = require('../models/Reservation');
const Packagetour = require('../models/Packagetour')
const moongose = require('mongoose')
//const verify = require('../routes/verifyToken');
// nlp
const natural = require('natural');
const {reservationValidation} = require('../routes/validation');

// GET ALL INTENTS
const getReservations = async (req,res) => {
  let search = req.query.search;
  let page =  (req.query.page == 'null' || req.query.page == undefined) ?  1 : req.query.page;
  let limit = (req.query.limit == 'null' || req.query.page == undefined) ?  5 : req.query.limit;

  try{
    const packages = await Packagetour.find().populate('categoryId')
      await Reservation.find(( search == 'null' || search == undefined ) ? {} : {_id : search})
      .skip((limit*page)-limit)
      .limit(limit)
      .collation({ locale: 'en', strength: 2 })
      .exec(function(err, reservations) {
        Reservation.countDocuments().exec(function(err, count) {
            res.status(200).send({
                reservations: reservations,
                packages : packages,
                current: page,
                pages: Math.ceil(count / limit)
            })
        })
    })
    
  }catch(err){
    res.status(400).send(err);
  }
}

// GET ALL INTENTS
const getReservationById = async (req,res) => {
  try{
    const data = await Reservation.findOne({_id : req.params.reservationId});
    res.status(200).send(data);
  }catch(err){
    res.status(400).send(err);
  }
}

// ADD INTENT 
const addReservation = async (req,res) => {

  const reservation = new Reservation({
    packageId : moongose.Schema.Types.ObjectId(req.body.packageId) ,
    customer : req.body.customer,
    start_date : req.body.start_date,
    duration : req.body.duration,
    no_adult : req.body.no_adult,
    no_child : req.body.no_child,
    preferences : req.body.preferences,
    notes : req.body.notes,
    status : req.body.status
  })

  try {
    const checkEmail = await Reservation.findOne({ "customer.email" : req.body.email} )
    if(checkEmail){
      if(checkEmail.status != 'Confirmed' || checkEmail.status != 'Canceled'){
        res.status(200).send({message : `You have already reserved, we will reach you soon!`, _id : null})
      }else{
        const saveData = await reservation.save();
        res.status(200).send({message : `Successfully add ${saveData._id}`, _id : saveData._id})
      }     
    }else{
      // INPUT DATA
      const saveData = await reservation.save();
      // EMAIL
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.GMAIL,
          pass: process.env.GMAIL_PASS
        }
      });
      console.log(process.env.GMAIL);    
      var mailOptions = {
        from: process.env.GMAIL,
        to: 'muhammad.rafiudin@students.amikom.ac.id',
        subject: 'New Reservation From Customer!',
        html: 'Please confirm reservation from <br> Name : '+req.body.customer.fullname+'<br> Reservation ID : '+saveData._id+'<br> Thank You!'
      };
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
      res.status(200).send({message : `Successfully add ${saveData._id}`, _id : saveData._id})
    }  
  }catch(err){
    res.status(400).json({
      message : err.message
    })
  }
}

// delete
const deleteReservation = async (req,res) => {
  
  try{
    const deleteData = await Reservation.remove({
      _id : req.params.reservationId
    })
    if(deleteData.deletedCount == 0) {
       res.status(200).send({message : "No data is deleted!"})
    }else{
      res.status(200).send({message : `Success deleted Id ${req.params.reservationId}`})
    }
    
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// UPDATE RESERVATION
const updateReservation = async (req,res) => {
  console.log(req.body);
 
  req.body.customer = {
    _id : moongose.Types.ObjectId(req.body.customerId),
    fullname : req.body.fullname,
    email : req.body.email,
    location : req.body.location,
    phone_number : req.body.phone_number
  }

  var date = new Date().toISOString().split('T')[0];

  try{
    if(req.body.updatePkg){
      const updateReservation = await Reservation.updateOne({
        _id : req.params.reservationId
      },
      {
        $set : {
            packageId : req.body.packageId,
            updatedAt : date
        }
      }
    );
    }else{
      const {error} = reservationValidation(req.body);
       if(error) return res.status(400).send(error.details[0].message );
      const updateReservation = await Reservation.updateOne({
        _id : req.params.reservationId
      },
      {
        $set : {
            packageId : req.body.packageId,
            start_date : req.body.start_date,
            customer : req.body.customer,
            duration : req.body.duration,
            no_adult : req.body.no_adult,
            no_child : req.body.no_child,
            preferences : req.body.preferences,
            notes : req.body.notes,
            status : req.body.status,
            updatedAt : date
        }
      }
    );
    }    
  res.status(200).json({message : `Successfully Update ${req.params.reservationId}`})
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
} 

// GET Recommendation
const getRecommendation = async (req,res) => {
  try{
    const packages = await Packagetour.find()
    const userReservation = await Reservation.findOne({ _id : req.body.reservationId})
    var userPref = userReservation.preferences

    //prepare data
    var pre_data = [];
    var num_word_allclass = 0
    var tokenizer = new natural.WordTokenizer();
    packages.map(function(data){
      for( var i in data.tags){
        pre_data.push({ class : data._id , training_data : data.tags })
      }
     })

    //tokenize and stemmed
    var token_data = []
    for( var i in pre_data){
      pre_data[i].training_data = tokenizer.tokenize(pre_data[i].training_data);
      var word_stemmed = []

      pre_data[i].training_data.map(function(data){
          word_stemmed.push(natural.LancasterStemmer.stem(data))
          token_data.push(natural.LancasterStemmer.stem(data))
      })
      var stemmed_data = []
      var filteredDupes = word_stemmed.filter((e, i, a) => a.indexOf(e) === i)
      //var num_of_words_in_class_comonality = filteredDupes.length
      var num_of_words_in_class = word_stemmed.length
      // var findDups = word_stemmed.filter((e, i, a) => a.indexOf(e) !== i)
      filteredDupes.map(function(data){
        num_word_allclass += 1;
        var numOfword = word_stemmed.filter(function(x){ return x === data; }).length;
        stemmed_data.push({word: data, count : numOfword, prob : numOfword/num_of_words_in_class  })
      })
      pre_data[i].training_data = stemmed_data;
      pre_data[i]['num_of_words'] = num_of_words_in_class // non comonality
      pre_data[i]['prob_class'] = 1/ pre_data.length
    }
    var num_of_commonality_word_allclass = token_data.filter((e, i, a) => a.indexOf(e) === i).length;

    // user
    var stemmed_qst = tokenizer.tokenize(userPref);
    for ( var i in stemmed_qst){
      stemmed_qst[i] = natural.LancasterStemmer.stem(stemmed_qst[i])
    }
    // filterd user input remove commonality ( optional)
    //const filtered_stemmed_qst = stemmed_qst.filter((e, i, a) => a.indexOf(e) === i)
    //console.log(filtered_stemmed_qst);
    var result_temp = []
    for ( var i in pre_data){
      var data_prob = []
      stemmed_qst.map(function(data){
        const index_found =pre_data[i].training_data.findIndex(e=> e.word == data)
        if(index_found>=0){
          data_prob.push(pre_data[i].training_data[index_found].count)
        }else{
          data_prob.push(0)
        }

      })
      result_temp.push({class: pre_data[i].class ,freq : data_prob , num_words :pre_data[i].num_of_words, class_prob : pre_data[i].prob_class})
      //const shouldLaplace = result_temp[pre_data[i].class].freq.findIndex(e => e == 0)

        for ( var i in result_temp ){
          var result_lapalace = 1;
          for ( var j in result_temp[i].freq){
              result_lapalace *= ((result_temp[i].freq[j]+1)/(result_temp[i].num_words+num_of_commonality_word_allclass));
          }
          result_lapalace *= result_temp[i].class_prob
          result_temp[i]['result'] = result_lapalace
        }
    }
    var recommendation = sortJSON(result_temp,'result','DESC')[0]
    const packageRecommendation = await Packagetour.findOne({ _id : recommendation.class})
    .populate('categoryId')
    .populate({
      path : 'itinerary',
      populate : 'destinationId'
    })
    res.status(200).json({
      reservation : userReservation,
      recommendation : packageRecommendation,
    })
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

function sortJSON(arr, key, way) {
  return arr.sort(function(a, b) {
      var x = a[key]; var y = b[key];
      if (way === 'ASC') { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
      if (way === 'DESC') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
  });
}

const validateForm = ( req,res,next) => {
  console.log(req.body);
  var customer = {
    fullname : req.body.fullname,
    email : req.body.email,
    phone_number : req.body.phone_number,
    location : req.body.location
  }

  req.body.customerId = ""
  req.body.customer = customer

  const {error} = reservationValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message );

  next()
}


module.exports = {
  getReservations : getReservations,
  getReservationById : getReservationById,
  addReservation : addReservation,
  deleteReservation : deleteReservation,
  updateReservation : updateReservation,
  getRecommendation : getRecommendation,
  validateForm : validateForm
};
