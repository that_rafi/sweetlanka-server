const express = require('express');
const Packagetour = require('../models/Packagetour');
const PackageCategory = require('../models/PackageCategory');
const Destination = require('../models/Destination');
//const verify = require('../routes/verifyToken');
// nlp
const {packagetourValidation,packageCategoryValidation} = require('../routes/validation');

// GET ALL INTENTS
const getPackagetours = async (req,res) => {
  let search = req.query.search;
  let page =  (req.query.page == 'null' || req.query.page == undefined) ?  1 : req.query.page;
  let limit = (req.query.limit == 'null' || req.query.page == undefined) ?  5 : req.query.limit;

  try{
      await Packagetour.find(( search == 'null' || search == undefined ) ? {} : {name : search})
      .skip((limit*page)-limit)
      .limit(limit)
      .collation({ locale: 'en', strength: 2 })
      .exec(function(err, packagetours) {
        Packagetour.countDocuments().exec(function(err, count) {
            res.status(200).send({
                data: packagetours,
                current: page,
                pages: Math.ceil(count / limit)
            })
        })
    })
    
  }catch(err){
    res.status(400).send(err);
  }
}

// GET ALL INTENTS
const getPackagetourById = async (req,res) => {
  try{
    const categories = await PackageCategory.find()
    const destinations = await Destination.find()
    var data = null;
    if(req.params.packagetourId != 'null'){
       data = await Packagetour.findOne({_id : req.params.packagetourId});
    }  
    res.status(200).send({
      package : data,
      category : categories,
      destination : destinations
    });
  }catch(err){
    res.status(400).send(err);
  }
}

// ADD INTENT 
const addPackagetour = async (req,res) => {
  
  var status = (req.body.status == 1) ? true : false

  console.log(req.body);

  const {error} = packagetourValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message );
  
  const packagetour = new Packagetour({
    name : req.body.name,
    desc : req.body.desc,
    facility : req.body.facility,
    base_price : req.body.base_price,
    categoryId : req.body.categoryId,
    hotel_star : req.body.hotel_star,
    start_date : req.body.start_date,
    pickup_location : req.body.pickup_location,
    tags : req.body.tags,
    status : status
  })

 // res.status(200).send(packagetour)

  try {
    const saveData = await packagetour.save();
    res.status(200).send({message : `Successfully add ${saveData._id}`, _id : saveData._id})
  }catch(err){
    res.status(400).json({
      message : err.message
    })
  }
}

// delete
const deletePackagetour = async (req,res) => {
  
  try{
    const deleteData = await Packagetour.remove({
      _id : req.params.packagetourId
    })
    if(deleteData.deletedCount == 0) {
       res.status(200).send({message : "No data is deleted!"})
    }else{
      res.status(200).send({message : `Success deleted Id ${req.params.packagetourId}`})
    }
    
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// UPDATE PACKAGETOUR
const updatePackagetour = async (req,res) => {
  var status = (req.body.status == 1) ? true : false
   try{
    if(req.body.updateitinerary){
      await Packagetour.updateOne({
              _id : req.params.packagetourId
            },
            {
              $set : {
                  itinerary : req.body.itinerary,
              }
            }
          );
    }else{
      const {error} = packagetourValidation(req.body);
      if(error) return res.status(400).send(error.details[0].message );

       await Packagetour.updateOne({_id : req.params.packagetourId},
          {
            $set : {
                name : req.body.name,
                desc : req.body.desc,
                facility : req.body.facility,
                base_price : req.body.base_price,
                pickup_location : req.body.pickup_location,
                categoryId : req.body.categoryId,
                hotel_star : req.body.hotel_star,
                start_date : req.body.start_date,
                tags : req.body.tags,
                status : status
            }
          }
        );
    }
  res.status(200).json({message : `Successfully Update ${req.params.packagetourId}`})
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
} 



module.exports = {
  getPackagetours : getPackagetours,
  getPackagetourById : getPackagetourById,
  addPackagetour : addPackagetour,
  deletePackagetour : deletePackagetour,
  updatePackagetour : updatePackagetour
};
