// Import the package
const express = require('express');
const router = express.Router();
const Users = require('../models/Users');
// VALIDATION
const bcrypt = require('bcryptjs')
const {userValidation,loginValidation} = require('../routes/validation');
// SESSION
const jwt = require('jsonwebtoken');

// GET ALL USERS
const getUsers = async (req,res) => {
  try{
    const users = await Users.find();
    res.status(200).json(users)
  }catch(err){
    res.status(400).json({
      messsage : err
    })
  }
}
// GET USER BY NAME
const getUsersByName = async (req,res) => {
  try{
    const user = await Users.findById(req.params.userId)
    res.status(200).json(user)
  }catch(error){
    res.status(400).json({
        message : error
    })
  }
}
// ADD USERS
const addUser = async (req,res) =>{
  // validate
  const {error} = userValidation(req.body)
  if (error) return res.status(400).send(error.details[0].message)

  try{
    // if email exist
    const emailExist = await Users.findOne({email : req.body.email})
    if(emailExist) return res.status(400).send("Email is already exsist")
    // if success
    // hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password,salt)
    const user = new Users({
        username : req.body.username,
        email : req.body.email,
        password : hashedPass
    })
    // save to DB
    const saveUser = await user.save();
    res.status(200).json(saveUser)
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}
// DELETE USERRS
const deleteUser = async ( req,res) =>{
  try{
    const deleteUser = await Users.remove({
      _id : req.params.userId
    })
    res.status(200).json(deleteUser)
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}
// UPDATE USERS
const updateUser = async (req,res) => {
  try{
    const updateUser = await Users.updateOne({
      _id : req.params.userId
    },
    {
      $set : {
        username : req.body.username,
        password : req.body.password
      }
    }
  );
  res.status(200).json(updateUser)
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}
// LOGIN
const loginUser = async (req,res) => {
    // Form Validation
    const {error} = loginValidation(req.body)
    if (error) return res.status(400).send(error.details[0].message)
  try{
    // check email exist
    const user = await Users.findOne({email:req.body.email})
    if(!user) return res.status(400).send("Email is not exists!")
    // check pass = email
    const EncodedPass = await bcrypt.compare(req.body.password,user.password)
    if(!EncodedPass) return res.status(400).send("Password is invalid!")
    // Create TOKEN and store id in jwt
    const token = jwt.sign({_id : user._id},process.env.TOKEN_SECRET)
    res.header('auth-user',token).status(200).send({ token : token})
  }catch(err){
    res.status(400).json({ message : err})
  }
}

module.exports = {
  getUsers : getUsers,
  getUsersByName : getUsersByName,
  addUser : addUser,
  deleteUser : deleteUser,
  updateUser : updateUser,
  loginUser : loginUser
};
