const express = require('express');
const PackageCategory = require('../models/PackageCategory');
//const verify = require('../routes/verifyToken');
// nlp
const {packageCategoryValidation} = require('../routes/validation');

// GET ALL INTENTS
const getPackageCategorys = async (req,res) => {
  let search = req.query.search;
  let page =  (req.query.page == 'null' || req.query.page == undefined) ?  1 : req.query.page;
  let limit = (req.query.limit == 'null' || req.query.page == undefined) ?  5 : req.query.limit;

  try{
      await PackageCategory.find(( search == 'null' || search == undefined ) ? {} : {name : search})
      .skip((limit*page)-limit)
      .limit(limit)
      .collation({ locale: 'en', strength: 2 })
      .exec(function(err, packagecategorys) {
        PackageCategory.countDocuments().exec(function(err, count) {
            res.status(200).send({
                data: packagecategorys,
                current: page,
                pages: Math.ceil(count / limit)
            })
        })
    })
    
  }catch(err){
    res.status(400).send(err);
  }
}

// GET ALL INTENTS
const getPackageCategoryById = async (req,res) => {
  try{
    const data = await PackageCategory.findOne({_id : req.params.categoryId});
    res.status(200).send(data);
  }catch(err){
    res.status(400).send(err);
  }
}

// ADD INTENT 
const addPackageCategory = async (req,res) => {
 
  console.log(req.body);

  const packagecategory = new PackageCategory({
    name : req.body.name,
    transportation : req.body.transportation,
    max_person : req.body.max_person
  })

 // res.status(200).send(packagecategory)

  const {error} = packageCategoryValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message );

  try {
    const saveData = await packagecategory.save();
    res.status(200).send({message : `Successfully add ${saveData._id}`, _id : saveData._id})
  }catch(err){
    res.status(400).json({
      message : err.message
    })
  }
}

// delete
const deletePackageCategory = async (req,res) => {
  
  try{
    const deleteData = await PackageCategory.remove({
      _id : req.params.categoryId
    })
    if(deleteData.deletedCount == 0) {
       res.status(200).send({message : "No data is deleted!"})
    }else{
      res.status(200).send({message : `Success deleted Id ${req.params.categoryId}`})
    }
    
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// UPDATE PACKAGECATEGORY
const updatePackageCategory = async (req,res) => {

  console.log(req.body);
  try{
      const {error} = packageCategoryValidation(req.body);
       if(error) return res.status(400).send(error.details[0].message );
      const updatePackageCategory = await PackageCategory.updateOne({
        _id : req.params.categoryId
      },
      {
        $set : {
            name : req.body.name,
            transportation : req.body.transportation,
            max_person : req.body.max_person
        }
      }
    );
    
    
  res.status(200).json({message : `Successfully Update ${req.params.categoryId}`})
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
} 



module.exports = {
  getPackageCategorys : getPackageCategorys,
  getPackageCategoryById : getPackageCategoryById,
  addPackageCategory : addPackageCategory,
  deletePackageCategory : deletePackageCategory,
  updatePackageCategory : updatePackageCategory
};
