const express = require('express');
const Destination = require('../models/Destination');
//const verify = require('../routes/verifyToken');
// nlp

const {destinationValidation} = require('../routes/validation');

// GET ALL INTENTS
const getDestinations = async (req,res) => {
  let search = req.query.search;
  let page =  (req.query.page == 'null' || req.query.page == undefined) ?  1 : req.query.page;
  let limit = (req.query.limit == 'null' || req.query.page == undefined) ?  5 : req.query.limit;

  try{
      await Destination.find(( search == 'null' || search == undefined ) ? {} : {name : search})
      .skip((limit*page)-limit)
      .limit(limit)
      .collation({ locale: 'en', strength: 2 })
      .exec(function(err, destinations) {
        Destination.countDocuments().exec(function(err, count) {
            res.status(200).send({
                data: destinations,
                current: page,
                pages: Math.ceil(count / limit)
            })
        })
    })
    
  }catch(err){
    res.status(400).send(err);
  }
}

// GET ALL INTENTS
const getDestinationById = async (req,res) => {
  try{
    const data = await Destination.findOne({_id : req.params.destinationId});
    res.status(200).send(data);
  }catch(err){
    res.status(400).send(err);
  }
}

// ADD INTENT 
const addDestination = async (req,res) => {
 
  console.log(req.body);

  const destination = new Destination({
    name : req.body.name,
    location : req.body.location,
    desc : req.body.desc,
    images : req.body.images
  })

 // res.status(200).send(destination)

  const {error} = destinationValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message );

  try {
    const saveData = await destination.save();
    res.status(200).send({message : `Successfully add ${saveData._id}`, _id : saveData._id})
  }catch(err){
    res.status(400).json({
      message : err.message
    })
  }
}

// delete
const deleteDestination = async (req,res) => {
  try{
    const deleteData = await Destination.remove({
      _id : req.params.destinationId
    })
    if(deleteData.deletedCount == 0) {
       res.status(200).send({message : "No data is deleted!"})
    }else{
      res.status(200).send({message : `Success deleted Id ${req.params.destinationId}`})
    }
    
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// UPDATE DESTINATION
const updateDestination = async (req,res) => {

  console.log(req.body);
  try{
    if(req.body.uploadImg){
      const updateDestination = await Destination.updateOne({
        _id : req.params.destinationId
      },
      {
        $set : {
          images : req.body.images
        }
      }
    );
    }else{
      const {error} = destinationValidation(req.body);
       if(error) return res.status(400).send(error.details[0].message );
      const updateDestination = await Destination.updateOne({
        _id : req.params.destinationId
      },
      {
        $set : {
          name : req.body.name,
          location : req.body.location,
          desc : req.body.desc,
          images : req.body.images
        }
      }
    );
    }
    
  res.status(200).json({message : `Successfully Update ${req.params.destinationId}`})
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
} 



module.exports = {
  getDestinations : getDestinations,
  getDestinationById : getDestinationById,
  addDestination : addDestination,
  deleteDestination : deleteDestination,
  updateDestination : updateDestination
};
