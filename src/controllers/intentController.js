const express = require('express');
const router = express.Router();
const Intent = require('../models/Intent');
const verify = require('../routes/verifyToken');
// nlp
const natural = require('natural');

const {intentValidation} = require('../routes/validation');

// GET ALL INTENTS
const getIntents = async (req,res) => {
  try{
    const data = await Intent.find();
    res.status(200).send(data);
  }catch(err){
    res.status(400).send(err);
  }
}

// ADD INTENT 
const addIntent = async (req,res) => {
  var array = []
  array.push(req.body.response)
  req.body.response = array
  req.body.training_data = JSON.parse(req.body.training_data)
  console.log(req.body.training_data);

  const {error} = intentValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message);
  const intent = new Intent({
    name : req.body.name,
    training_data : req.body.training_data,
    response : req.body.response
  })
  try {
    const saveData = await intent.save();
    res.status(200).send({message : `Successfully Update ${saveData._id}`})
  }catch(err){
    res.status(400).json({
      message : err.message
    })
  }
}

const updateIntent = async (req,res) => {
  try{
    var array = []
    array.push(req.body.response)
    req.body.response = array
    req.body.training_data = JSON.parse(req.body.training_data)
    console.log(req.body.training_data);
    const updateIntent = await Intent.updateOne({
      _id : req.params.intentId
    },
    {
      $set : {
        name : req.body.name,
        response : req.body.response,
        training_data : req.body.training_data,
      }
    })
    res.status(200).send({message : `Successfully Update ${req.params.intentId}`})
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// delete
const deleteIntent = async (req,res) => {
  try{
    const deleteData = await Intent.remove({
      _id : req.params.intentId
    })
    res.status(200).send(deleteData)
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// test 
const testIntent = async (req,res) => {
  const bayesClassify = new natural.BayesClassifier();
  try{
    var qst = req.body.question
    //console.log(qst);
    const result = await Intent.find();
    // with bayes
    // result.map(function(data){
    //   data.training_data.map(function(res){
    //     bayesClassify.addDocument(res.sentence,data.name)
    //   })
    // })
    // bayesClassify.train()
    // const findRespond = await Intent.findOne({name:bayesClassify.classify(qst)})
    //
    // bayesClassify.save('test.json', (err, classifier) => {
    // if (err) {
    //   throw new Exception(err);
    // }
    // });
    //
    // console.log(bayesClassify.getClassifications(qst));


    //prepare data
    var pre_data = [];
    var convert_data = [];
    var num_sentence_allclass = 0;
    var num_word_allclass = 0
    var tokenizer = new natural.WordTokenizer();
    result.map(function(data){
       data.training_data.map(function(res){
         num_sentence_allclass += 1
         convert_data.push({class : data.name, training_data : res.sentence,response : data.response })
      })
     })


     for(var i = 0; i < convert_data.length; i++){
      const existingClass = pre_data.find(d => d.class == convert_data[i].class);

      if(existingClass){
        existingClass.training_data += ','+convert_data[i].training_data
      } else{
        pre_data.push({
            class: convert_data[i].class,
          training_data: convert_data[i].training_data,
          response : convert_data[i].response
        });
      }
    }
    // tokenize and stemmed
    var token_data = []
    for( var i in pre_data){
      const num_sentence_inclass = pre_data[i].training_data.split(",").length;
      pre_data[i].training_data = tokenizer.tokenize(pre_data[i].training_data);
      var word_stemmed = []
      var count_token = 1

      pre_data[i].training_data.map(function(data){
          word_stemmed.push(natural.LancasterStemmer.stem(data))
          token_data.push(natural.LancasterStemmer.stem(data))
      })
      var stemmed_data = []
      var filteredDupes = word_stemmed.filter((e, i, a) => a.indexOf(e) === i)
      //var num_of_words_in_class_comonality = filteredDupes.length
      var num_of_words_in_class = word_stemmed.length
      // var findDups = word_stemmed.filter((e, i, a) => a.indexOf(e) !== i)
      filteredDupes.map(function(data){
        num_word_allclass += 1;
        var numOfword = word_stemmed.filter(function(x){ return x === data; }).length;
        stemmed_data.push({word: data, count : numOfword, prob : numOfword/num_of_words_in_class  })
      })
      pre_data[i].training_data = stemmed_data;
      pre_data[i]['num_of_words'] = num_of_words_in_class // non comonality
      pre_data[i]['prob_class'] = num_sentence_inclass / num_sentence_allclass
    }
    var num_of_commonality_word_allclass = token_data.filter((e, i, a) => a.indexOf(e) === i).length;

    // user
    var stemmed_qst = tokenizer.tokenize(qst);
    for ( var i in stemmed_qst){
      stemmed_qst[i] = natural.LancasterStemmer.stem(stemmed_qst[i])
    }
    // filterd user input remove commonality ( optional)
    // const filtered_stemmed_qst = stemmed_qst.filter((e, i, a) => a.indexOf(e) === i)
    // console.log(filtered_stemmed_qst);
    var result_temp = []
    for ( var i in pre_data){
      var data_prob = []
      stemmed_qst.map(function(data){
        const index_found =pre_data[i].training_data.findIndex(e=> e.word == data)
        if(index_found>=0){
          data_prob.push(pre_data[i].training_data[index_found].count)
        }else{
          data_prob.push(0)
        }

      })
      result_temp.push({class: pre_data[i].class ,freq : data_prob , num_words :pre_data[i].num_of_words, class_prob : pre_data[i].prob_class, response : pre_data[i].response})
      //const shouldLaplace = result_temp[pre_data[i].class].freq.findIndex(e => e == 0)

        for ( var i in result_temp ){
          var result_lapalace = 1;
          for ( var j in result_temp[i].freq){
              result_lapalace *= ((result_temp[i].freq[j]+1)/(result_temp[i].num_words+num_of_commonality_word_allclass));
          }
          result_lapalace *= result_temp[i].class_prob
          result_temp[i]['result'] = result_lapalace
        }
    }
    var response = sortJSON(result_temp,'result','DESC')[0]
    res.status(200).json({ class : response.class , response : response.response[0], scor : response.result})
  }catch(err){
    res.status(400).send({message:err})
  }
}


function sortJSON(arr, key, way) {
  return arr.sort(function(a, b) {
      var x = a[key]; var y = b[key];
      if (way === 'ASC') { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
      if (way === 'DESC') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
  });
}


module.exports = {
  getIntents : getIntents,
  addIntent : addIntent,
  deleteIntent : deleteIntent,
  testIntent : testIntent,
  updateIntent : updateIntent
};
