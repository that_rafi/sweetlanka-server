const express = require('express');
const Article = require('../models/Article');
//const verify = require('../routes/verifyToken');
// nlp

const {articleValidation} = require('../routes/validation');

// GET ALL INTENTS
const getArticles = async (req,res) => {
  let search = req.query.search;
  let page =  (req.query.page == 'null' || req.query.page == undefined) ?  1 : req.query.page;
  let limit = (req.query.limit == 'null' || req.query.page == undefined) ?  5 : req.query.limit;

  try{
      await Article.find(( search == 'null' || search == undefined ) ? {} : {title : search})
      .skip((limit*page)-limit)
      .limit(limit)
      .collation({ locale: 'en', strength: 2 })
      .exec(function(err, articles) {
        Article.countDocuments().exec(function(err, count) {
            res.status(200).send({
                data: articles,
                current: page,
                pages: Math.ceil(count / limit)
            })
        })
    })
    
  }catch(err){
    res.status(400).send(err);
  }
}

// GET ALL INTENTS
const getArticleById = async (req,res) => {
  try{
    const data = await Article.findOne({_id : req.params.articleId});
    res.status(200).send(data);
  }catch(err){
    res.status(400).send(err);
  }
}

// ADD INTENT 
const addArticle = async (req,res) => {
 
  console.log(req.body);

  const article = new Article({
    title : req.body.title,
    author : req.body.author,
    desc : req.body.desc,
    images : req.body.images
  })

 // res.status(200).send(article)

  const {error} = articleValidation(req.body);
  if(error) return res.status(400).send(error.details[0].message );

  try {
    const saveData = await article.save();
    res.status(200).send({message : `Successfully add ${saveData._id}`, _id : saveData._id})
  }catch(err){
    res.status(400).json({
      message : err.message
    })
  }
}

// delete
const deleteArticle = async (req,res) => {
  try{
    const deleteData = await Article.remove({
      _id : req.params.articleId
    })
    if(deleteData.deletedCount == 0) {
       res.status(200).send({message : "No data is deleted!"})
    }else{
      res.status(200).send({message : `Success deleted Id ${req.params.articleId}`})
    }
    
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
}

// UPDATE ARTICLE
const updateArticle = async (req,res) => {

  console.log(req.body);
  try{
    if(req.body.uploadImg){
      const updateArticle = await Article.updateOne({
        _id : req.params.articleId
      },
      {
        $set : {
          images : req.body.images
        }
      }
    );
    }else{
      const {error} = articleValidation(req.body);
       if(error) return res.status(400).send(error.details[0].message );
       var date = new Date().toISOString().split('T')[0];

      const updateArticle = await Article.updateOne({
        _id : req.params.articleId
      },
      {
        $set : {
          title : req.body.title,
          desc : req.body.desc,
          author : req.body.author,
          images : req.body.images,
          updatedAt : date
        }
      }
    );
    }
    
  res.status(200).json({message : `Successfully Update ${req.params.articleId}`})
  }catch(err){
    res.status(400).json({
      message : err
    })
  }
} 



module.exports = {
  getArticles : getArticles,
  getArticleById : getArticleById,
  addArticle : addArticle,
  deleteArticle : deleteArticle,
  updateArticle : updateArticle
};
