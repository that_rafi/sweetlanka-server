const moongose = require('mongoose')

const categoryScheme = new moongose.Schema({
    name : {
        type : String,
        required : true
    },
    max_person : {
        type : Number,
        required : true
    },
    transportation : {
        type : String,
        required : true
    }
    // dont forget to input destination
},{timestamps : true})


module.exports = moongose.model('PackageCategory', categoryScheme)