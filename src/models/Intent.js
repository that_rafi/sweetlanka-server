const mongoose = require('mongoose');

const IntentSchema = mongoose.Schema({
    name : {
      type : String,
      required : true
    },
    training_data :{
      type : [{
        sentence : {
          type : String
        }
      }]
    },
    response :{
      type : [String]
    }
},{timestamps : true});

module.exports = mongoose.model('Intent',IntentSchema);
