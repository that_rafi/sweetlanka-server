const moongose = require('mongoose')

const DestinationScheme = moongose.Schema({
    name : {
        type : String,
        required : true
    },
    location : {
        type : String,
        required : true
    },
    desc : {
        type : String,
    },
    images : {
        type : [moongose.Schema.Types.Mixed]
    }
},{timestamps : true});

module.exports = moongose.model('Destination',DestinationScheme)