const moongose = require('mongoose')
const {packagetourValidation} = require('../routes/validation');

const itineraryScheme = new moongose.Schema({
    date : {
        type : Date
    },
    title : {
        type : String,
    },
    program : {
        type : String
    },
    destinationId : {
        type : moongose.Schema.Types.ObjectId,
        ref: "Destination",
    }
    // dont forget to input destination
})

const PackagetourScheme = moongose.Schema({
    name : {
        type : String,
        required : true
    },
    desc : {
        type : String,
        validate:(req,res)=>{
            const {error} = packagetourValidation(req.body);
            if(error) return false         
        },
        required : true
    },
    facility : {
        type : [String],
        required : true
    },
    base_price : {
        type : Number,
        required : true
    },
    hotel_star : {
        type : String,
        required : true
    },
    categoryId : {
        type : moongose.Schema.Types.ObjectId,
        ref: "PackageCategory",
        required : true
    },
    pickup_location : {
        type : String,
        required : false
    },
    tags : {
        type : String,
        required : true
    },
    start_date : {
        type : Date,
        required : true
    },
    itinerary : {
        type :[itineraryScheme]
    },
    status : {
        type : Boolean,
        required : true
    }
},{timestamps : true})

module.exports = moongose.model('Packagetour', PackagetourScheme)