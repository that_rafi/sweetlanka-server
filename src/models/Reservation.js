const moongose = require('mongoose')

const customerScheme = new moongose.Schema({
    fullname : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    phone_number : {
        type : String,
        required : true
    },
    location : {
        type : String,
        required : true
    }
    // dont forget to input destination
})

const reservationScheme = moongose.Schema({
    packageId : {
        type : moongose.Schema.Types.ObjectId,
    },
    customer : {
        type : customerScheme,
        required : true
    },
    start_date : {
        type : Date,
        required : true
    },
    duration : {
        type : Number,
        required : true
    },
    no_adult : {
        type : Number,
        required : true
    },
    no_child : {
        type : Number,
        required : true
    },
    preferences : {
        type : String,
        required : true
    },
    notes : {
        type : String,
    },
    status : {
        type : String,
        required : true
    }
},{timestamps : true})

module.exports = moongose.model('Reservation', reservationScheme)