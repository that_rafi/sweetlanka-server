const moongose = require('mongoose')

const ArticleScheme = moongose.Schema({
    title : {
        type : String,
        required : true
    },
    desc : {
        type : String,
        required : true
    },
    author : {
        type : String,
        required : true
    },
    images : {
        type : [moongose.Schema.Types.Mixed]
    }
},{timestamps : true});

module.exports = moongose.model('Article',ArticleScheme)