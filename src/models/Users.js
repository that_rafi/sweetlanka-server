const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
    username : {
      type : String,
      required : true
    },
    email :{
      type : String,
      required : true,
      min : 6,
      max : 255
    },
    password : {
      type : String,
      required : true
    }
},{timestamps : true});

module.exports = mongoose.model('Users',UsersSchema);
