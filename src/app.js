require('dotenv/config');
// Import the package
const bodyParser = require('body-parser');
const path = require('path');
const express = require('express');
const mongoose = require('mongoose');

// execute or initiate
const app = express()
const http = require('http').createServer(app);
//app.use(express.json());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PATCH, DELETE");
next();
});


// Middleware (function that execute when a specific route r being hit)
// usually we pass authentification here ...use('/',auth)
// app.use('/',()=>{
//   console.log('This is Middleware');
// })


// exmaple of Middleware
// OLD VER
// const usersRoute = require('./routes/usersController');
// const intentRoute = require('./routes/intentController');
// app.use('/users',usersRoute)
// app.use('/intent',intentRoute)
// NEW VER
const initWebRoutes = require("./routes/webRoute")
initWebRoutes(app)

// ROUTES
app.use(express.static(path.join(__dirname + '/public')))

// socket io for chat
const io = require('socket.io')(http);

io.on('connection',(socket)=>{
  var name = ""
  var chats = {}
  socket.emit('message','welcome to sweetlanka!');
  socket.emit('req-name','Would you like to tell me your name ?')
  // versus socket.broadcast.emit (broadcast to all of the client except the client that is connecting)
  //socket.broadcast.emit('message','A new user has joined the chat!')
  // to inform all user that ther is a new user join the chat


  socket.on('disconnect',()=>{
    // if user discconnect
  })

  socket.on('user-name',(name)=>{
    var room = socket.id
    var natural = require('natural');
    var tokenizer = new natural.WordTokenizer();
    var nameArray = tokenizer.tokenize(name);
    chats = {
      id : room,
      username : name,
      chat : []
    }
    io.to(room).emit('message','Hi, nice to meet you '+nameArray[nameArray.length-1]+' , What can i do for you?')
  })

  socket.on('user-msg',(msg)=>{
    chats.chat.push(msg);
    console.log(chats);
    // query disini
    // check ada id room / tidak
    // kalau tidak create new kalau ada update chats.chat.push
  })

})


// Connect
mongoose.connect( process.env.DB_CONNECTION ,
{ useNewUrlParser: true,useUnifiedTopology: true },
()=>{
  console.log('connect to db');
})

// how to start listening to the server
let port = process.env.PORT || 3000;
http.listen(port,()=>{
  console.log(`Server is running in port ${port}`);
})
